#include <iostream>
using namespace std;

/*Napisać program, który wczytuje macierze kwadratowe liczb całkowitych A i B wymiaru n (n podaje użytkownik z klawiatury), 
wypisuje jej elementy na ekranie monitora, 
a następnie program oblicza sumę oraz iloczyn tych macierzy i wypisuje w ten sposób uzyskane macierze na ekranie.*/

int main()
{  
    int n;
    cout << "Podaj wymiar macierzy: ";
    cin >> n;
    
    int macierz_A[n][n], macierz_B[n][n];
    
    for(int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << "Podaj [" << i+1 << ", " << j+1 << "] macierzy A: ";
            cin >> macierz_A[i][j];
        }
    }
    
    for(int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << "Podaj [" << i+1 << ", " << j+1 << "] macierzy B: ";
            cin >> macierz_B[i][j];
        }
    }

    cout << "Macierz A: " << endl;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << macierz_A[i][j] << " ";
        } 
        cout << endl;
    }
    
    cout << "Macierz B: " << endl;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << macierz_B[i][j] << " ";
        } 
        cout << endl;
    }
    
    // żeby oszczędzić pamięć, macierze sumy i iloczynu nie są zapisywane, a tylko wyświetlane na bieżąco
    
    cout << "Suma macierzy: " << endl;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << macierz_A[i][j] + macierz_B[i][j] << " ";
        }
        cout << endl;
    }
    
    cout << "Iloczyn macierzy: " << endl;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << macierz_A[i][j] * macierz_B[i][j] << " ";
        }
        cout << endl;
    }
            
		
    return 0;
}

